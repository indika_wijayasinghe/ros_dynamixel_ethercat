
#include "EasyCAT.h"        
//#include "Dynamixel.h"               
#include <SPI.h> 

  
//---- AB&T EasyCAT library V_1.4 -----------------------------------------------------------------	
//
// AB&T Tecnologie Informatiche - Ivrea Italy
// http://www.bausano.net
// https://www.ethercat.org/en/products/791FFAA126AD43859920EA64384AD4FD.htm
//
// This library has been tested with Arduino IDE 1.6.8
// https://www.arduino.cc

//--- V_1.4 ---
//
// The MainTask function now return the state of the 
// Ethercat State machine and of the Wachdog
//
// Now the SPI chip select is initialized High inside the constructor


//--- V_1.3 --- 
// Replaced delay(100) in Init() function with a wait loop 
//
//
//--- V_1.2 --- 
// SPI chip select is configured by the application, setting a constructor parameter.
// If no chip select is declared in the constructor, pin 9 will be used as default.  
// Code cleaning.
// Comments in english.
// TODO: fast SPI transfert for DUE board
//
//
//--- V_1.1 --- 
// First official release.
// SPI chip select is configured editing the library ".h" file.
    
//---- constructor --------------------------------------------------------------------------------

Dynamixel DYN;
byte IDs[2] = {0x01, 0x02};

EasyCAT::EasyCAT(unsigned char SPI_CHIP_SELECT) //------- SPI_CHIP_SELECT options -----------------
                                                //
                                                // for EasyCAT board REV_A we can choose between:
                                                // 8, 9, 10
                                                //  
                                                // for EasyCAT board REV_B we have three additional options:
                                                // A5, 6, 7 

{                                       
  SCS = (unsigned char)SPI_CHIP_SELECT;         //  initialize chip select  
  digitalWrite (SCS, HIGH);                     //    
}
 
 
EasyCAT::EasyCAT()                              //------- default constructor ---------------------- 
{                                               //                               
  SCS = 9;                                      // if no chip select is declared, default is pin 9 
  digitalWrite (SCS, HIGH);                     // initialize chip select     
}                                               //

  
//---- EasyCAT board initialization ---------------------------------------------------------------

bool EasyCAT::Init()
{
  ULONG TempLong;

  SPI.begin();
  
  #if defined(ARDUINO_ARCH_SAMD)                          // calculate the microcontroller port and
    Bit_SCS = g_APinDescription[SCS].ulPin;               // pin for fast SPI chip select management     
    Port_SCS = g_APinDescription[SCS].ulPort;             //                                           
  #endif                                                  //  
                                                          //  
  #if defined(ARDUINO_ARCH_SAM)                           //    
    Bit_SCS = g_APinDescription[SCS].ulPin;               //    
    pPort_SCS = g_APinDescription[SCS].pPort;             //  
  #endif                                                  //
                                                          //
  #if defined(ARDUINO_ARCH_AVR)                           //
    Mask_SCS = (digitalPinToBitMask(SCS));                //  
    Port_SCS = digitalPinToPort(SCS);                     //
    pPort_SCS = portOutputRegister(Port_SCS);             //     
  #endif                                                  //
    
  digitalWrite(SCS, HIGH);
  pinMode(SCS, OUTPUT);   
  
  SPI.beginTransaction(SPISettings(SpiSpeed, MSBFIRST, SPI_MODE0)); // set SPI parameters

  SPIWriteRegisterDirect(0x054, 0x15000101);
  SPIWriteRegisterDirect(0x05C, 1);
 
  SPIWriteRegisterDirect (RESET_CTL, (DIGITAL_RST & ETHERCAT_RST)); // LAN9252 reset
  
  for (unsigned int i=0; i<50000; i++)
  {
	  digitalWrite(SCS, HIGH);
  }
  
  TempLong.Long = SPIReadRegisterDirect (BYTE_TEST, 4);             // read test register  
  
  if (TempLong.Long == 0x87654321)                                  // if the test register is ok 
  {                                                                 // check also the READY flag
    TempLong.Long = SPIReadRegisterDirect (HW_CFG, 4);              //    
     if (TempLong.Long & READY)                                     //
     {                                                              //
        SPI.endTransaction();                                       // if both are ok

        byte ID[1] = {0x00};
        ID[0] = {0x01};
        int Val = {0x01};
        Dynamixel::SetTorqueMode(ID, Val);
        delay(2);
        ID[0] = {0x02};
        Dynamixel::SetTorqueMode(ID, Val);
        delay(2);
        
        return true;                                                // initalization completed    
     }
   }
  
  SPI.endTransaction();                                             // 
  
  SPI.end();                                                        // otherwise                                                                      //
  
  return false;                                                     // initialization failed           
}

void EasyCAT::MyFunction()
{
  bool WatchDog = 0;
  bool Operational = 1; 
  unsigned char i;
  ULONG TempLong; 
  unsigned char Status;  
                                                          
  SPI.beginTransaction(SPISettings(SpiSpeed, MSBFIRST, SPI_MODE0));
  
  unsigned long val = SPIReadRegisterDirect(0x058, 4) & 0x0f;
  Serial.print("SysT: ");
  Serial.println(val);

  SPI.endTransaction();

  /*byte ID[1] = {0x00};
  ID[0] = {0x01};
  int Val = {0x01};
  Dynamixel::SetTorqueMode(ID, Val);
  delay(2);
  ID[0] = {0x02};
  Dynamixel::SetTorqueMode(ID, Val);
  delay(2);*/

  int EncVal[2] = {3*1024, 3*1024};

  Dynamixel::SetGoalPositionBulk(IDs, EncVal);
}


//---- EtherCAT task ------------------------------------------------------------------------------

unsigned char EasyCAT::MainTask()                           // must be called cyclically by the application

{
  //unsigned long ec_time;
  //ec_time = micros();
  
  bool WatchDog = 0;
  bool Operational = 1; 
  unsigned char i;
  ULONG TempLong; 
  unsigned char Status;  
                                                            // set SPI parameters
  SPI.beginTransaction(SPISettings(SpiSpeed, MSBFIRST, SPI_MODE0)); 
 
  
  TempLong.Long = SPIReadRegisterIndirect (WDOG_STATUS, 1); // read watchdog status
  if ((TempLong.Byte[0] & 0x01) == 0x01)                    //
    WatchDog = 0;                                           // set/reset the corrisponding flag
  else                                                      //
    WatchDog = 1;                                           //
    
  TempLong.Long = SPIReadRegisterIndirect (AL_STATUS, 1);   // read the EtherCAT State Machine status
  Status = TempLong.Byte[0] & 0x0F;                         //
  if (Status == ESM_OP)                                     // to see if we are in operational state
    Operational = 1;                                        //
  else                                                      // set/reset the corrisponding flag
    Operational = 0;                                        //    
                                                            //--- process data transfert ----------
                                                            //                                                        
  /*if (WatchDog | !Operational)                              // if watchdog is active or we are 
  {                                                         // not in operational state, reset 
    for (i=0; i<4; i++)                                     // the output buffer
    BufferOut.Long[i] = 0;                                  //
   
  }*/
  /*else                                                      
  {                                                         
    SPIReadProcRamFifo();                                   // otherwise transfer process data from 
    BufferIn.Byte[7] = BufferOut.Byte[0]+1;
  }             */                                           // the EtherCAT core to the output buffer  

  SPIReadProcRamFifo();                                   // otherwise transfer process data from 
  /*BufferIn.Byte[0] = BufferOut.Byte[0]+1;
  BufferIn.Byte[1] = BufferOut.Byte[1]+1;
  BufferIn.Byte[7] = BufferOut.Byte[0]+1;
  BufferIn.Byte[31] = 0xff;*/

  /*Serial.print("OP: ");
  Serial.println(Operational);*/
 
  if (BufferOut.Byte[31] == 0x01)
  {
    started = 1;
    int pos[2];
    digitalWrite(5, LOW);
    Dynamixel::GetPresentPositionBulkBlocking(IDs, pos, 2);
    
    BufferIn.Byte[0] = pos[0] & 0xff;
    BufferIn.Byte[1] = (pos[0] >> 8);
    BufferIn.Byte[2] = pos[1] & 0xff;
    BufferIn.Byte[3] = (pos[1] >> 8);

    BufferIn.Byte[31] = Operational;
  }
  else if (BufferOut.Byte[31] == 0x02 && Operational && started)
  {
    int trq[2];
    trq[0] = BufferOut.Byte[0] + (BufferOut.Byte[1] << 8);
    trq[1] = BufferOut.Byte[2] + (BufferOut.Byte[3] << 8);

    digitalWrite(5, HIGH);
    Dynamixel::SetGoalTorqueBulk(IDs, trq);
  }

  if (!Operational)
  {
    started = 0;
  }  

  SPIWriteProcRamFifo();                                    // we always transfer process data from
                                                            // the input buffer to the EtherCAT core  
                                                            
  SPI.endTransaction();                                     //

  if (WatchDog)                                             // return the status of the State Machine      
  {                                                         // and of the watchdog
    Status |= 0x80;                                         //
  }                                                         //

  //Serial.println(micros()-ec_time);
  
  return Status;                                            //  
}

    
//---- read a directly addressable registers  -----------------------------------------------------

unsigned long EasyCAT::SPIReadRegisterDirect (unsigned short Address, unsigned char Len)

                                                   // Address = register to read
                                                   // Len = number of bytes to read (1,2,3,4)
                                                   //
                                                   // a long is returned but only the requested bytes
                                                   // are meaningful, starting from LsByte                                                 
{
  ULONG Result; 
  UWORD Addr;
  Addr.Word = Address; 
  unsigned char i; 
  
  SCS_Low_macro                                             // SPI chip select enable

  SPI_TransferTx(COMM_SPI_READ);                            // SPI read command
  SPI_TransferTx(Addr.Byte[1]);                             // address of the register
  SPI_TransferTxLast(Addr.Byte[0]);                         // to read, MsByte first
 
  for (i=0; i<Len; i++)                                     // read the requested number of bytes
  {                                                         // LsByte first 
    Result.Byte[i] = SPI_TransferRx(DUMMY_BYTE);            //
  }                                                         //    
  
  SCS_High_macro                                            // SPI chip select disable 
 
  return Result.Long;                                       // return the result
}


//---- write a directly addressable registers  ----------------------------------------------------

void EasyCAT::SPIWriteRegisterDirect (unsigned short Address, unsigned long DataOut)

                                                   // Address = register to write
                                                   // DataOut = data to write
{ 
  ULONG Data; 
  UWORD Addr;
  Addr.Word = Address;
  Data.Long = DataOut;    

  SCS_Low_macro                                             // SPI chip select enable  
  
  SPI_TransferTx(COMM_SPI_WRITE);                           // SPI write command
  SPI_TransferTx(Addr.Byte[1]);                             // address of the register
  SPI_TransferTx(Addr.Byte[0]);                             // to write MsByte first

  SPI_TransferTx(Data.Byte[0]);                             // data to write 
  SPI_TransferTx(Data.Byte[1]);                             // LsByte first
  SPI_TransferTx(Data.Byte[2]);                             //
  SPI_TransferTxLast(Data.Byte[3]);                         //
 
  SCS_High_macro                                            // SPI chip select enable   
}


//---- read an undirectly addressable registers  --------------------------------------------------

unsigned long EasyCAT::SPIReadRegisterIndirect (unsigned short Address, unsigned char Len)

                                                   // Address = register to read
                                                   // Len = number of bytes to read (1,2,3,4)
                                                   //
                                                   // a long is returned but only the requested bytes
                                                   // are meaningful, starting from LsByte                                                  
{
  ULONG TempLong;
  UWORD Addr;
  Addr.Word = Address;
                                                            // compose the command
                                                            //
  TempLong.Byte[0] = Addr.Byte[0];                          // address of the register
  TempLong.Byte[1] = Addr.Byte[1];                          // to read, LsByte first
  TempLong.Byte[2] = Len;                                   // number of bytes to read
  TempLong.Byte[3] = ESC_READ;                              // ESC read 

  SPIWriteRegisterDirect (ECAT_CSR_CMD, TempLong.Long);     // write the command

  do
  {                                                         // wait for command execution
    TempLong.Long = SPIReadRegisterDirect(ECAT_CSR_CMD,4);  //
  }                                                         //
  while(TempLong.Byte[3] & ECAT_CSR_BUSY);                  //
                                                             
                                                              
  TempLong.Long = SPIReadRegisterDirect(ECAT_CSR_DATA,Len); // read the requested register
  return TempLong.Long;                                     //
}


//---- write an undirectly addressable registers  -------------------------------------------------

void  EasyCAT::SPIWriteRegisterIndirect (unsigned long DataOut, unsigned short Address)

                                                   // Address = register to write
                                                   // DataOut = data to write                                                    
{
  ULONG TempLong;
  UWORD Addr;
  Addr.Word = Address;


  SPIWriteRegisterDirect (ECAT_CSR_DATA, DataOut);            // write the data

                                                              // compose the command
                                                              //                                
  TempLong.Byte[0] = Addr.Byte[0];                            // address of the register  
  TempLong.Byte[1] = Addr.Byte[1];                            // to write, LsByte first
  TempLong.Byte[2] = 4;                                       // we write always 4 bytes
  TempLong.Byte[3] = ESC_WRITE;                               // ESC write

  SPIWriteRegisterDirect (ECAT_CSR_CMD, TempLong.Long);       // write the command

  do                                                          // wait for command execution
  {                                                           //
    TempLong.Long = SPIReadRegisterDirect (ECAT_CSR_CMD, 4);  //  
  }                                                           //  
  while (TempLong.Byte[3] & ECAT_CSR_BUSY);                   //
  
}


//---- read from process ram fifo ----------------------------------------------------------------

void EasyCAT::SPIReadProcRamFifo()    // read 32 bytes from the output process ram, through the fifo
                                      //        
                                      // these are the bytes received from the EtherCAT master and
                                      // that will be use by our application to write the outputs
{
  ULONG TempLong;
  unsigned char i;
  
  SPIWriteRegisterDirect (ECAT_PRAM_RD_ADDR_LEN, 0x00201000);   // we always read 32 bytes   0x0020----
                                                                // output process ram offset 0x----1000
                                                                
  SPIWriteRegisterDirect (ECAT_PRAM_RD_CMD, 0x80000000);        // start command                                                                  
                                                                                                                               
  do                                                            // wait for data to be transferred      
  {                                                             // from the output process ram 
    TempLong.Long = SPIReadRegisterDirect (ECAT_PRAM_RD_CMD,4); // to the read fifo 
  }                                                             //    
  while (!(TempLong.Byte[0] & PRAM_READ_AVAIL) || (TempLong.Byte[1] != 8));     
  
 
  SCS_Low_macro                                                 // SPI chip select enable  
  
  SPI_TransferTx(COMM_SPI_READ);                                // SPI read command
  SPI_TransferTx(0x00);                                         // address of the read  
  SPI_TransferTxLast(0x00);                                     // fifo MsByte first
  

  for (i=0; i<32; i++)                                          // 32 bytes read loop 
  {                                                             // 
    BufferOut.Byte[i] = SPI_TransferRx(DUMMY_BYTE);             //
  }                                                             //
    
  SCS_High_macro                                                // SPI chip select disable    
}


//---- write to the process ram fifo --------------------------------------------------------------

void EasyCAT::SPIWriteProcRamFifo()    // write 32 bytes to the input process ram, through the fifo
                                       //    
                                       // these are the bytes that we have read from the inputs of our                   
                                       // application and that will be sent to the EtherCAT master
{
  ULONG TempLong;
  unsigned char i;

  SPIWriteRegisterDirect (ECAT_PRAM_WR_ADDR_LEN, 0x00201200);   // we always write 32 bytes 0x0020----
                                                                // input process ram offset 0x----1200

  SPIWriteRegisterDirect (ECAT_PRAM_WR_CMD, 0x80000000);        // start command  

  do                                                            // check fifo has available space     
  {                                                             // for data to be written
    TempLong.Long = SPIReadRegisterDirect (ECAT_PRAM_WR_CMD,4); //  
  }                                                             //    
  while (!(TempLong.Byte[0] & PRAM_WRITE_AVAIL) || (TempLong.Byte[1] < 8) );             
  
  
  SCS_Low_macro                                                 // enable SPI chip select
  
  SPI_TransferTx(COMM_SPI_WRITE);                               // SPI write command
  SPI_TransferTx(0x00);                                         // address of the write fifo 
  SPI_TransferTx(0x20);                                         // MsByte first 

  for (i=0; i<31; i++)                                          // 32 bytes write loop
  {                                                             //
    SPI_TransferTx (BufferIn.Byte[i]);                          // 
  }                                                             //
                                                                //  
  SPI_TransferTxLast (BufferIn.Byte[31]);                       //
  
  SCS_High_macro                                                // disable SPI chip select      
} 





