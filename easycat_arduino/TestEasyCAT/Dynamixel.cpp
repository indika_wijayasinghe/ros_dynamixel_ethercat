#include "Dynamixel.h"

byte BID[1] = {0xFE};

Dynamixel::Dynamixel()
{
  
}

byte Dynamixel::GetCheckSum(byte* u8_Packet, int count) 
{
  // count is length of the packet except the checksum  
  byte u8_Checksum = 0;    
  for (int i=2; i<count; i++) u8_Checksum += u8_Packet[i];    
  
  return ~u8_Checksum;
}

int Dynamixel::SendInstruction(byte* u8_ServoID, byte u8_Instruction, byte* u8_Params, int s32_ParamCount) 
{
  byte u8_TxPacket[100];
    // -------- SEND INSTRUCTION ------------
    int S=0;
    u8_TxPacket[S++] = 0xFF;
    u8_TxPacket[S++] = 0xFF;
    u8_TxPacket[S++] = u8_ServoID[0];
    u8_TxPacket[S++] = s32_ParamCount + 2;
    u8_TxPacket[S++] = u8_Instruction;    
    for (int i=0; i<s32_ParamCount; i++) {
        u8_TxPacket[S++] = u8_Params[i];
    }
    int S_t = S;
    u8_TxPacket[S++] = GetCheckSum(u8_TxPacket, S_t);
    Serial1.clear();
    Serial1EnableOpenDrain(false);
    Serial1.write(u8_TxPacket, S);
    Serial1.flush(); // waits until the last data byte has been sent
    Serial1EnableOpenDrain(true);    
    // -------- READ ECHO (timeout = 1 second) ------------
    uint32_t u32_Start = millis();
    int R = 0;
    while (R<S) {
        if ((millis() - u32_Start) > 1000) {
            //DEBUGOUT.println("Send ERROR: Timeout waiting for Echo (Rx and Tx must be tied togteher)");
            return -1;
        }        
        if (!Serial1.available()) continue;        
        if (Serial1.read() != u8_TxPacket[R++]) {
             //DEBUGOUT.println("ERROR: Invalid echo received");
            return -1;
        }
    } 
}

void Dynamixel::GetPresentPositionBulk(byte* u8_ServoID, const int NumActuators)
{
  byte u8_Data[3*NumActuators+1];
  u8_Data[0] = 0;

  for (int i = 0; i < NumActuators; i++) 
  {
    u8_Data[3*i+1] = 2;
    u8_Data[3*i+2] = u8_ServoID[i];
    u8_Data[3*i+3] = R_PresentPosition;
  }
    
  SendInstruction(BID, I_BulkRead, u8_Data, sizeof(u8_Data));
}

int Dynamixel::GetResponse(byte* u8_RxPacket, int packet_length) 
{
    uint32_t u32_Start = millis();
    int R = 0;    
    while (R<packet_length) {
        if ((millis() - u32_Start) > 1) {
            //DEBUGOUT.println("-1");
            return -1;
        }        
        if (!Serial1.available()) continue;
        u8_RxPacket[R++] = Serial1.read();
    }
    if (u8_RxPacket[R-1] != GetCheckSum(u8_RxPacket, R-1)) {
        //DEBUGOUT.println("ERROR: Invalid checksum in the response packet");
        return -1;
    }

    return 0;
}

void Dynamixel::GetPresentPositionBulkBlocking(byte* u8_ServoID, int* PosVals, const int NumActuators) 
{
  byte u8_Data[3*NumActuators+1];
  u8_Data[0] = 0;
  int err;

  for (int i = 0; i < NumActuators; i++) 
  {
    u8_Data[3*i+1] = 2;
    u8_Data[3*i+2] = u8_ServoID[i];
    u8_Data[3*i+3] = R_PresentPosition;
  }

  SendInstruction(BID, I_BulkRead, u8_Data, sizeof(u8_Data));
  
  byte u8_RxPacket[8];

  for (int i = 0; i < NumActuators; i++) 
  {
    err = GetResponse(u8_RxPacket, 8);

    if (!err)
    {
      PosVals[i] = (int)((u8_RxPacket[6] << 8) + u8_RxPacket[5]);
    }
    else
    {
      PosVals[i] = -1;
    }
  }
}

bool Dynamixel::SetResponseTime(byte* u8_ServoID, int Val) 
{
    byte u8_Data[2];
    byte u8_RxPacket[6];
    u8_Data[0] = R_ReturnDelayTime;
    u8_Data[1] = (byte)(Val&0xFF);
    SendInstruction(u8_ServoID, I_WriteData, u8_Data, sizeof(u8_Data));
    return true;
}

bool Dynamixel::SetTorqueMode(byte* u8_ServoID, int Val) 
{
    byte u8_Data[2];
    byte u8_RxPacket[6];
    u8_Data[0] = R_TRQ_CONT_MOD;
    u8_Data[1] = (byte)(Val&0xFF);
    SendInstruction(u8_ServoID, I_WriteData, u8_Data, sizeof(u8_Data));
    return true;
}

bool Dynamixel::SetGoalPositionBulk(byte* u8_ServoID, int* EncVal) 
{
    byte u8_Data[8];
    byte u8_RxPacket[6];
    u8_Data[0] = R_GoalPosition;
    u8_Data[1] = 2;
    u8_Data[2] = u8_ServoID[0];
    u8_Data[3] = (byte)(EncVal[0]&0xFF);
    u8_Data[4] = (byte)((EncVal[0] >> 8)&0xFF);
    u8_Data[5] = u8_ServoID[1];
    u8_Data[6] = (byte)(EncVal[1]&0xFF);
    u8_Data[7] = (byte)((EncVal[1] >> 8)&0xFF);
    SendInstruction(BID, I_SyncWrite, u8_Data, sizeof(u8_Data));
    return true;
}

bool Dynamixel::SetGoalTorqueBulk(byte* u8_ServoID, int* TrqVal)
{
    byte u8_Data[8];
    byte u8_RxPacket[6];
    u8_Data[0] = R_GOAL_TORQUE;
    u8_Data[1] = 2;
    u8_Data[2] = u8_ServoID[0];
    u8_Data[3] = (byte)(TrqVal[0]&0xFF);
    u8_Data[4] = (byte)((TrqVal[0] >> 8)&0xFF);
    u8_Data[5] = u8_ServoID[1];
    u8_Data[6] = (byte)(TrqVal[1]&0xFF);
    u8_Data[7] = (byte)((TrqVal[1] >> 8)&0xFF);
    SendInstruction(BID, I_SyncWrite, u8_Data, sizeof(u8_Data));
    return true;
}

void Dynamixel::Serial1EnableOpenDrain(bool bEnable) {
    if (bEnable) CORE_PIN1_CONFIG = PORT_PCR_DSE | PORT_PCR_ODE | PORT_PCR_MUX(3); // set Open Drain Enabled
    else         CORE_PIN1_CONFIG = PORT_PCR_DSE | PORT_PCR_SRE | PORT_PCR_MUX(3); // set Slew Rate Enabled
}
