
#ifndef ABT_EasyCAT_H
#define ABT_EasyCAT_H

#include <Arduino.h> 
#include <SPI.h> 
#include "Dynamixel.h"

//---- AB&T EasyCAT library V_1.4 -----------------------------------------------------------------	
//
// AB&T Tecnologie Informatiche - Ivrea Italy
// http://www.bausano.net
// https://www.ethercat.org/en/products/791FFAA126AD43859920EA64384AD4FD.htm
//
// This library has been tested with Arduino IDE 1.6.8
// https://www.arduino.cc



//****** configuration parameters *****************************************************************

 #define SPI_fast_transfer                 // enable fast SPI transfert (default)
 #define SPI_fast_SS                       // enable fast SPI chip select management (default)
 

//*************************************************************************************************


//---- LAN9252 registers --------------------------------------------------------------------------

                                            //---- access to EtherCAT registers -------------------

#define ECAT_CSR_DATA           0x0300      // EtherCAT CSR Interface Data Register
#define ECAT_CSR_CMD            0x0304      // EtherCAT CSR Interface Command Register


                                            //---- access to EtherCAT process RAM ----------------- 

#define ECAT_PRAM_RD_ADDR_LEN   0x0308      // EtherCAT Process RAM Read Address and Length Register
#define ECAT_PRAM_RD_CMD        0x030C      // EtherCAT Process RAM Read Command Register
#define ECAT_PRAM_WR_ADDR_LEN   0x0310      // EtherCAT Process RAM Write Address and Length Register 
#define ECAT_PRAM_WR_CMD        0x0314      // EtherCAT Process RAM Write Command Register

#define ECAT_PRAM_RD_DATA       0x0000      // EtherCAT Process RAM Read Data FIFO
#define ECAT_PRAM_WR_DATA       0x0020      // EtherCAT Process RAM Write Data FIFO

                                            //---- EtherCAT registers -----------------------------
                                            
#define AL_STATUS               0x0130      // AL status
#define WDOG_STATUS             0x0440      // watch dog status


                                            //---- LAN9252 registers ------------------------------    

#define HW_CFG                  0x0074      // hardware configuration register
#define BYTE_TEST               0x0064      // byte order test register
#define RESET_CTL               0x01F8      // reset register       
#define ID_REV                  0x0050      // chip ID and revision


//---- LAN9252 flags ------------------------------------------------------------------------------

#define ECAT_CSR_BUSY     0x80
#define PRAM_READ_BUSY    0x80
#define PRAM_READ_AVAIL   0x01
#define PRAM_WRITE_AVAIL  0x01
#define READY             0x08000000

#define DIGITAL_RST       0x00000001
#define ETHERCAT_RST      0x00000040


//---- EtherCAT flags -----------------------------------------------------------------------------

                                                      // EtherCAT state machine 
                                                      
#define ESM_INIT                0x01                  // init
#define ESM_PREOP               0x02                  // pre-operational
#define ESM_BOOT                0x03                  // bootstrap
#define ESM_SAFEOP              0x04                  // safe-operational
#define ESM_OP                  0x08                  // operational


//--- ESC commands --------------------------------------------------------------------------------

#define ESC_WRITE 		   0x80
#define ESC_READ 		     0xC0


//---- SPI ----------------------------------------------------------------------------------------

#define COMM_SPI_READ    0x03
#define COMM_SPI_WRITE   0x02

#define DUMMY_BYTE       0xFF



/*#if defined(ARDUINO_ARCH_AVR)  
  #define SpiSpeed         8000000
  
#elif defined (ARDUINO_ARCH_SAM)
  #define SpiSpeed        14000000 
  
#elif defined (ARDUINO_ARCH_SAMD)
  #define SpiSpeed        12000000   
  
#else  
  #define SpiSpeed        8000000    
  //#define SpiSpeed        14000000   
#endif*/

#define SpiSpeed        24000000
  
  
//---- typedef ------------------------------------------------------------------------------------

typedef union
{
    unsigned short  Word;
    unsigned char   Byte[2];
} UWORD;

typedef union
{
    unsigned long   Long;
    unsigned short  Word[2];
    unsigned char   Byte[4];
} ULONG;

typedef union
{
  unsigned char Byte [32];
  unsigned long Long [8];      
} PROCBUFFER;


//-------------------------------------------------------------------------------------------------
 
class EasyCAT 
{
  public:
    EasyCAT(unsigned char SPI_CHIP_SELECT); // constructor
    EasyCAT();                              // default constructor

    
    unsigned char MainTask();               // EtherCAT main task
    void          MyFunction();
                                            // must be called cyclically by the application 
    
    bool Init();                            // EasyCAT board initialization 
    
    PROCBUFFER BufferOut;                   // output process data buffer 
    PROCBUFFER BufferIn;                    // input process data buffer

    unsigned int started = 0;
    
    
  private:
    void          SPIWriteRegisterDirect   (unsigned short Address, unsigned long DataOut);
    unsigned long SPIReadRegisterDirect    (unsigned short Address, unsigned char Len);
    
    void          SPIWriteRegisterIndirect (unsigned long  DataOut, unsigned short Address);
    unsigned long SPIReadRegisterIndirect  (unsigned short Address, unsigned char Len); 
    
    void          SPIReadProcRamFifo();    
    void          SPIWriteProcRamFifo();  
    
    unsigned char SCS; 

    #if defined(ARDUINO_ARCH_SAM)    
      Pio* pPort_SCS; 
      uint32_t Bit_SCS;      
    #endif

    #if defined(ARDUINO_ARCH_SAMD)    
      EPortType Port_SCS;
      uint32_t Bit_SCS;       
    #endif    
    
    #if defined(ARDUINO_ARCH_AVR)    
      unsigned char Mask_SCS;
      unsigned char Port_SCS;
      volatile uint8_t* pPort_SCS;       
    #endif        
    
 
 
 //----- fast SPI chip select management ----------------------------------------------------------
 
    #if defined SPI_fast_SS 

      #if defined(ARDUINO_ARCH_AVR)                   // -- AVR architecture (Uno - Mega) ---------
        #define SCS_Low_macro      *pPort_SCS &= ~(Mask_SCS);
        #define SCS_High_macro     *pPort_SCS |=  (Mask_SCS); 
          
      //#elif defined(ARDUINO_ARCH_SAMD)                //--- SAMD architecture (Zero) --------------
      //  #define SCS_Low_macro      PORT->Group[Port_SCS].OUTCLR.reg = (1<<Bit_SCS);
      //  #define SCS_High_macro     PORT->Group[Port_SCS].OUTSET.reg = (1<<Bit_SCS);
        
      #elif defined(ARDUINO_ARCH_SAM)                 //---- SAM architecture (Due) ---------------
      	#define SCS_Low_macro      pPort_SCS->PIO_CODR = Bit_SCS;
        #define SCS_High_macro     pPort_SCS->PIO_SODR = Bit_SCS;

      #else                                    //-- standard management for others architectures -- 
        #define SCS_Low_macro      digitalWrite(SCS, LOW);
        #define SCS_High_macro     digitalWrite(SCS, HIGH);        

      #endif    
  
  
 //----- standard SPI chip select management ------------------------------------------------------

    #else     
      #define SCS_Low_macro     digitalWrite(SCS, LOW);
      #define SCS_High_macro    digitalWrite(SCS, HIGH);  
    #endif  
     
//-------------------------------------------------------------------------------------------------    
    
    
    
    
//----- fast SPI transfert ------------------------------------------------------------------------
    
  #if defined SPI_fast_transfer      

    #if defined(ARDUINO_ARCH_AVR)                     // -- AVR architecture (Uno - Mega) ---------

      inline static void SPI_TransferTx(unsigned char Data) {                             \
                                                            SPDR = Data;                  \
                                                            asm volatile("nop");        
                                                            while (!(SPSR & _BV(SPIF))) ; \
                                                            };               
          
      inline static void SPI_TransferTxLast(unsigned char Data) {                         \
                                                            SPDR = Data;                  \
                                                            asm volatile("nop");                                                                        
                                                            while (!(SPSR & _BV(SPIF))) ; \
                                                            };         
       
      inline static unsigned char SPI_TransferRx(unsigned char Data) {                      \
                                                              SPDR = Data;                  \
                                                              asm volatile("nop");          
                                                              while (!(SPSR & _BV(SPIF))) ; \
                                                              return SPDR; };    
      
    #elif defined(ARDUINO_ARCH_SAMD)                    //--- SAMD architecture (Zero) --------------
     
      inline static void SPI_TransferTx (unsigned char Data){                                     \
                                                    while(SERCOM4->SPI.INTFLAG.bit.DRE == 0){};   \
                                                    SERCOM4->SPI.DATA.bit.DATA = Data;}; 
                                                     
      inline static void SPI_TransferTxLast(unsigned char Data){                                  \
                                                    while(SERCOM4->SPI.INTFLAG.bit.DRE == 0){};   \
                                                    SERCOM4->SPI.DATA.bit.DATA = Data;            \
                                                    while(SERCOM4->SPI.INTFLAG.bit.TXC == 0){};};                                                      
                                                                  
      inline static unsigned char SPI_TransferRx (unsigned char Data){                                \
                                                    unsigned char Dummy = SERCOM4->SPI.DATA.bit.DATA; \
                                                    while(SERCOM4->SPI.INTFLAG.bit.DRE == 0){};       \
                                                    SERCOM4->SPI.DATA.bit.DATA = Data;                \
                                                    while(SERCOM4->SPI.INTFLAG.bit.RXC == 0){};       \
                                                    return SERCOM4->SPI.DATA.bit.DATA;};              \
                                                                                                     
    #elif defined(ARDUINO_ARCH_SAM)                   //---- SAM architecture (Due) ---------------   
                                                      // TODO! currently standard transfert is used  
       
        inline static void SPI_TransferTx          (unsigned char Data) {SPI.transfer(Data); };    
        inline static void SPI_TransferTxLast      (unsigned char Data) {SPI.transfer(Data); }; 
        inline static unsigned char SPI_TransferRx (unsigned char Data) {return SPI.transfer(Data); };        
       
       
    #else                                             //-- standard transfert for others architectures
    
      inline static void SPI_TransferTx            (unsigned char Data) {SPI.transfer(Data); };    
      inline static void SPI_TransferTxLast        (unsigned char Data) {SPI.transfer(Data); }; 
      inline static unsigned char SPI_TransferRx   (unsigned char Data) {return SPI.transfer(Data); }; 
 
    #endif        
      
//---- standard SPI transfert ---------------------------------------------------------------------  

  #else                              
      inline static void SPI_TransferTx          (unsigned char Data) {SPI.transfer(Data); };    
      inline static void SPI_TransferTxLast      (unsigned char Data) {SPI.transfer(Data); }; 
      inline static unsigned char SPI_TransferRx (unsigned char Data) {return SPI.transfer(Data); };         
  #endif       
 
//---------------------------------------------------------------------------------------- 
   
};
 
 
  #endif
