#include <SPI.h> 
/*#include <avr/io.h>
#include <avr/interrupt.h>*/
#include "EasyCAT.h"

unsigned long Millis = 0;
unsigned long PreviousSaw = 0;
unsigned long PreviousCycle = 0;
unsigned long stime = 0;
unsigned long myMillis = 0;
unsigned long curr = 0;
unsigned long prev = 0;
unsigned int locked = 0;

EasyCAT EASYCAT;

void setup()
{
  Serial.begin(2000000);
  Serial1.begin(2000000);
  
  Serial.println("Staring...");

  pinMode(9, OUTPUT);
  pinMode(1, OUTPUT);
  pinMode(2, INPUT);
  pinMode(5, OUTPUT);

  if (EASYCAT.Init() == true)                                     
  {                                                               
    Serial.println("Initialized");                       
  }                                                             
  else                                                              
  {                                                               
    Serial.print ("Initialization failed");                                                                             
  }

  if (digitalRead(2))
    EASYCAT.MainTask();
  
  EIMSK |= (1<<2);
  sei();

  attachInterrupt(2, intCallback, RISING); 
}

void intCallback()
{
  EASYCAT.MainTask();
  //Serial.println("Called");
}
 
void loop()                                            
{                                          
}
