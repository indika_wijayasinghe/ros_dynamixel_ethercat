#include <SPI.h>
#include <Wire.h>

//byte BID[1] = {0xFE};

enum eInstruction {
    I_Ping      = 1,
    I_ReadData  = 2,
    I_WriteData = 3,
    I_RegWrite  = 4,
    I_Action    = 5,
    I_Reset     = 6,
    I_SyncWrite = 0x83,
    I_BulkRead = 0x92,
};


enum eRegister {
    // ---------- EEPROM ------------
    R_ModelNumber             = 0x00, // 2 Byte
    R_FirmwareVersion         = 0x02, // 
    R_ServoID                 = 0x03, //         Write
    R_BaudRate                = 0x04, //         Write
    R_ReturnDelayTime         = 0x05, //         Write
    R_CW_AngleLimit           = 0x06, // 2 Byte  Write
    R_CCW_AngleLimit          = 0x08, // 2 Byte  Write
    R_HighestLimitTemperature = 0x0B, //         Write
    R_LowestLimitVoltage      = 0x0C, //         Write
    R_HighestLimitVoltage     = 0x0D, //         Write
    R_MaxTorque               = 0x0E, // 2 Byte  Write
    R_StatusReturnLevel       = 0x10, //         Write
    R_AlarmLED                = 0x11, //         Write
    R_AlarmShutdown           = 0x12, //         Write
    R_DownCalibration         = 0x14, // 2 Byte
    R_UpCalibration           = 0x16, // 2 Byte
    // ----------- RAM -------------
    R_TorqueEnable            = 0x18, //         Write
    R_LED                     = 0x19, //         Write
    R_CW_ComplianceMargin     = 0x1A, //         Write
    R_CCW_ComplianceMargin    = 0x1B, //         Write
    R_CW_ComplianceSlope      = 0x1C, //         Write
    R_CCW_ComplianceSlope     = 0x1D, //         Write
    R_GoalPosition            = 0x1E, // 2 Byte  Write
    R_MovingSpeed             = 0x20, // 2 Byte  Write
    R_TorqueLimit             = 0x22, // 2 Byte  Write
    R_PresentPosition         = 0x24, // 2 Byte
    R_PresentSpeed            = 0x26, // 2 Byte
    R_PresentLoad             = 0x28, // 2 Byte
    R_PresentVoltage          = 0x2A, //
    R_PresentTemperature      = 0x2B, //
    R_RegisteredInstruction   = 0x2C, //         Write
    R_Moving                  = 0x2E, //
    R_Lock                    = 0x2F, //         Write
    R_Punch                   = 0x30, // 2 Byte  Write
    R_CURRENT                 = 0x44, // 2 Byte  Write
    R_TRQ_CONT_MOD            = 0x46,
    R_GOAL_TORQUE             = 0x47, // 2 Byte  Write
    R_GOAL_ACCELERATION       = 0x49,
};


enum eError {
    E_InputVoltage   = 0x01,
    E_AngleLimit     = 0x02,
    E_Overheating    = 0x04,
    E_ParameterRange = 0x08,
    E_Checksum       = 0x10,
    E_Overload       = 0x20,
    E_Instruction    = 0x40,
};

class Dynamixel
{
  public:
    Dynamixel();

    static int SendInstruction(byte* u8_ServoID, byte u8_Instruction, byte* u8_Params, int s32_ParamCount); 
    static byte GetCheckSum(byte* u8_Packet, int count);
    static void GetPresentPositionBulk(byte* u8_ServoID, const int NumActuators);
    static int GetResponse(byte* u8_RxPacket, int packet_length);
    static void GetPresentPositionBulkBlocking(byte* u8_ServoID, int* PosVals, const int NumActuators);
    static bool SetResponseTime(byte* u8_ServoID, int Val);
    static bool SetGoalPositionBulk(byte* u8_ServoID, int* EncVal);
    static bool SetTorqueMode(byte* u8_ServoID, int Val);
    static bool SetGoalTorqueBulk(byte* u8_ServoID, int* TrqVal);

    /*static int GetResponse(byte*, int); 
    static void ReadPositionBulk(byte*, int*, const int);
    static void WritePositionBulk(byte*, int*);
    static void WriteTorqueBulk(byte*, int*);*/  

  private:
    static void Serial1EnableOpenDrain(bool bEnable);
};

