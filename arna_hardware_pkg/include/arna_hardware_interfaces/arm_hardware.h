/*
 *	Author: Indika Wijayasinghe
 *	Email: 	i.b.wijayasinghe@ieee.org
 */
 
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/joint_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include <ethercat_master/ethercat_master.h>

namespace arna_hardware_interfaces
{

class TwoLinkArm : public hardware_interface::RobotHW
{
public:
  	double cmd[2];
  	double pos[2];
  	double vel[2];
  	double eff[2];
  	int op_event = 0;
  	bool op_status = false;

	TwoLinkArm();
	~TwoLinkArm();
	
	// Read robot state variables
	void read();
	
	// Write robot commands
	void write();

private:
  	hardware_interface::JointStateInterface jnt_state_interface;
  	hardware_interface::PositionJointInterface jnt_pos_interface;
  	hardware_interface::EffortJointInterface jnt_effort_interface;
  	ethercat_master::EtherCATMaster eth_master;
};

}
