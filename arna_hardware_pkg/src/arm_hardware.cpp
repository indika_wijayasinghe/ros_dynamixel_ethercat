/*
 *	Author: Indika Wijayasinghe
 *	eth_masterail: 	i.b.wijayasinghe@ieee.org
 */
 
#include <arna_hardware_interfaces/arm_hardware.h>

namespace arna_hardware_interfaces
{
	TwoLinkArm::TwoLinkArm() 
 	{ 
   		// Connect and register the joint state interface
   		hardware_interface::JointStateHandle state_handle_a("joint1", &pos[0], &vel[0], &eff[0]);
   		jnt_state_interface.registerHandle(state_handle_a);

   		hardware_interface::JointStateHandle state_handle_b("joint2", &pos[1], &vel[1], &eff[1]);
   		jnt_state_interface.registerHandle(state_handle_b);

   		registerInterface(&jnt_state_interface);

   		// Connect and register the joint position interface
   		hardware_interface::JointHandle pos_handle_a(jnt_state_interface.getHandle("joint1"), &cmd[0]);
   		jnt_pos_interface.registerHandle(pos_handle_a);

   		hardware_interface::JointHandle pos_handle_b(jnt_state_interface.getHandle("joint2"), &cmd[1]);
   		jnt_pos_interface.registerHandle(pos_handle_b);

   		registerInterface(&jnt_pos_interface);
   		
   		// Connect and register the effort joint interface
   		hardware_interface::JointHandle effort_handle_a(jnt_state_interface.getHandle("joint1"), &cmd[0]);
   		jnt_effort_interface.registerHandle(effort_handle_a);
   		
   		hardware_interface::JointHandle effort_handle_b(jnt_state_interface.getHandle("joint2"), &cmd[1]);
   		jnt_effort_interface.registerHandle(effort_handle_b);
   		
   		registerInterface(&jnt_effort_interface);
  	}

	TwoLinkArm::~TwoLinkArm()
	{
		cmd[0] = 0;
		cmd[1] = 0;
		write();
	}
  	
  	// Implement real-time safe read from Dynamixels
  	void TwoLinkArm::read() 
  	{ 
  		/*
  		 *	Read position, velocity, and torque values from the motors (for all joints) and update the variables pos, vel, and eff, respectively. 
  		 *	These values are available in the controller update function (position_controller.cpp) through joint_.getPosition(), joint_.getVelocity(), and joint_.getEffort().
  		 */

  		eth_master.domain_pd[eth_master.off_out + 31] = 0x01;

  		eth_master.sendDatagram();
  		usleep(1200);
  		eth_master.receiveDatagram();

  		pos[0] = (double)(eth_master.domain_pd[eth_master.off_in + 0] + (eth_master.domain_pd[eth_master.off_in + 1] << 8))*0.001534;
  		pos[1] = (double)(eth_master.domain_pd[eth_master.off_in + 2] + (eth_master.domain_pd[eth_master.off_in + 3] << 8))*0.001534;

  		// Detect EtherCAT OP status change
  		if (op_status ^ (bool)eth_master.domain_pd[eth_master.off_in + 31])
  		{
  			op_status = (bool)eth_master.domain_pd[eth_master.off_in + 31];

  			if (op_status)
  				op_event = 1;
  			else
  				op_event = -1;
  		}
  		else
  		{
  			op_event = 0;
  		}
  	}
  	
  	// Implement real-time safe write to Dynamixels
  	void TwoLinkArm::write() 
  	{
  		/*
  		 *	Write cmd values to the motors as torque commands. 
  		 *	The values set in the controller update function (position_controller.cpp) using joint_.setCommand(double) appears here as cmd.
  		 */ 

  		unsigned int cmd_i[2] = {0, 0};

  		if (cmd[0] > 0)
  			cmd_i[0] = (unsigned int)(cmd[0]*121.8);
  		else
  			cmd_i[0] = (unsigned int)(-cmd[0]*121.8) + 1024;

  		if (cmd[1] > 0)
  			cmd_i[1] = (unsigned int)(cmd[1]*121.8);
  		else
  			cmd_i[1] = (unsigned int)(-cmd[1]*121.8) + 1024;

  		eth_master.domain_pd[eth_master.off_out + 0] = cmd_i[0] & 0xFF;
  		eth_master.domain_pd[eth_master.off_out + 1] = cmd_i[0] >> 8;
  		eth_master.domain_pd[eth_master.off_out + 2] = cmd_i[1] & 0xFF;
  		eth_master.domain_pd[eth_master.off_out + 3] = cmd_i[1] >> 8;
  		eth_master.domain_pd[eth_master.off_out + 31] = 0x02;

  		eth_master.sendDatagram();
  		usleep(200);
  		eth_master.receiveDatagram();
  	}
}
