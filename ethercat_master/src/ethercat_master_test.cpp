/*
 *	Author:	Indika Wijayasinghe
 *	Email:	i.b.wijayasinghe@ieee.org
 */

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <sstream>
#include <cmath>
#include <stdlib.h>
#include <time.h>
#include <sched.h>
#include <pthread.h>
#include <limits.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <ros/ros.h>
#include <ros/time.h>
#include <std_msgs/Int64.h>
#include <realtime_tools/realtime_publisher.h>
#include <ethercat_master/ecrt.h>
#include <ethercat_master/ethercat_master.h>
//#include <ethercat_master/easycat.h>

#include <ros/callback_queue.h>

/****************************************************************************/

#define MY_PRIORITY (49)
#define MAX_SAFE_STACK (8*1024) // The maximum stack size which is guaranteed safe to access without faulting
#define NSEC_PER_SEC (1000000000)
#define SPEID (4)	// CPU ID to pin the thread
#define FREQUENCY (200)

/****************************************************************************/

// Timer
static unsigned int sig_alarms = 0;
static unsigned int user_alarms = 0;

struct timespec t;
long prev, curr;
static int shut_down = 0;
static int ret_flag = 0;
static int started = 0;

realtime_tools::RealtimePublisher<std_msgs::Int64>* pub_1_;
realtime_tools::RealtimePublisher<std_msgs::Int64>* pub_2_;
/****************************************************************************/

static unsigned int blink = 0;
static unsigned int ret_blink = 0;
static int diff;

/*****************************************************************************/

static int latency_target_fd;

ethercat_master::EtherCATMaster em;

/*
 * Latency trick (Copied from cyclictest source code) if the file /dev/cpu_dma_latency exists, open it and write a zero into it. This will tell
 * the power management system not to transition to a high cstate (in fact, the system acts like idle=poll). When the fd to /dev/cpu_dma_latency
 * is closed, the behavior goes back to the system default. Documentation/power/pm_qos_interface.txt
 */

static void set_latency_target(void)
{
	struct stat s;
	int err;
	latency_target_fd = -1;
	int32_t latency_target_value = 0;

	errno = 0;
	err = stat("/dev/cpu_dma_latency", &s);

	if (err == -1)
	{
		perror("Could not stat /dev/cpu_dma_latency failed");
		return;
	}

	errno = 0;
	latency_target_fd = open("/dev/cpu_dma_latency", O_RDWR);

	if (latency_target_fd == -1)
	{
		perror("Could not open /dev/cpu_dma_latency");
		return;
	}

	errno = 0;
	err = write(latency_target_fd, &latency_target_value, 4);

	if (err < 1)
	{
		perror("error setting cpu_dma_latency to %d!");
		close(latency_target_fd);
		return;
	}

	printf("/dev/cpu_dma_latency set to %dus\n", latency_target_value);
}

static void setup_rt_requirements(void)
{
	struct sched_param param;
	unsigned char dummy[MAX_SAFE_STACK];

	cpu_set_t cpuset;
	pthread_t thread;

	thread = pthread_self();

	// Set affinity mask to include CPUs 0 to 7
	CPU_ZERO(&cpuset);
	CPU_SET(SPEID, &cpuset);

	if (pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuset) == -1)
	{
		perror("pthread_setaffinity failed");
	}

	if (pthread_getaffinity_np(thread, sizeof(cpu_set_t), &cpuset) == -1)
	{
		perror("pthread_getaffinity_np");
	}

	for (int j = 0; j < CPU_SETSIZE; j++)
	{
		if (CPU_ISSET(j, &cpuset))
		{
	   		fprintf(stderr,"%d CPU %d\n", SPEID, j);
		}
	}

	// Declare ourself as a real time task
	param.sched_priority = MY_PRIORITY;

	if (sched_setscheduler(0, SCHED_FIFO, &param) == -1)
	{
		perror("sched_setscheduler failed");
		exit(-1);
	}

	// Lock memory
	if (mlockall(MCL_CURRENT|MCL_FUTURE) == -1)
	{
		perror("mlockall failed");
	    exit(-2);
	}

	// Use the /dev/cpu_dma_latency trick available
	set_latency_target();

	// Pre-fault our stack
	memset(dummy, 0, MAX_SAFE_STACK);

	return;
}

static void clean_up()
{
	munlockall();

	if (latency_target_fd >= 0)
		close(latency_target_fd);

	return;
}

/****************************************************************************/

void cyclic_task()
{
	em.sendDatagram();
	usleep(1500);
	em.receiveDatagram();

	ret_blink = em.domain_pd[em.off_in + 7];

	diff = ret_blink - blink;

	if (diff == 1)
	{
		started = 1;
	}

	if (((diff == 1) || (diff == -255) || (started == 0)) && (ret_flag == 0))
	{
		ret_flag = 0;
	}
	else
	{
		if (!ret_flag)
			ret_flag = diff + 2;
	}

	blink = ret_blink;

	em.domain_pd[em.off_out] = blink;

	em.sendDatagram();

	if ((diff == 0) && (started == 1))
	{
		pub_1_->msg_.data = (long)(diff);
		pub_1_->unlockAndPublish();
	}

	pub_2_->msg_.data = (long)(ret_blink);
	pub_2_->unlockAndPublish();

	/*clock_gettime(CLOCK_MONOTONIC, &t);
	prev = 1000000000*t.tv_sec + t.tv_nsec;

    // send process data
    ecrt_domain_queue(domain1);
    ecrt_master_send(master);

    usleep(1500);

    ecrt_master_receive(master);
    ecrt_domain_process(domain1);

    ret_blink = (unsigned int)EC_READ_U8(domain1_pd + off_easycat_in + 7);

    diff = ret_blink - blink;

    if (diff == 1)
    {
    	started = 1;
    }

    //if (((ret_blink == blink + 1) || ((ret_blink == 0) && (blink == 255))) && ((ret_flag != 1) || (started == 0)))
    //if (((ret_blink == blink + 1) && (ret_flag != 1)) || (started == 0))
    if (((diff == 1) || (diff == -255) || (started == 0)) && (ret_flag == 0))
    {
    	ret_flag = 0;
    }
    else
    {
    	if (!ret_flag)
    		ret_flag = diff + 2;
    }

    blink = ret_blink;

    EC_WRITE_U8(domain1_pd + off_easycat_out, blink);

    ecrt_domain_queue(domain1);
    ecrt_master_send(master);

    //usleep(400);*/

    /*ecrt_master_receive(master);
    ecrt_domain_process(domain1);*/

    /*clock_gettime(CLOCK_MONOTONIC, &t);
    curr = 1000000000*t.tv_sec + t.tv_nsec;

    if ((diff == 0) && (started == 1))
    {
    	pub_1_->msg_.data = (long)(diff);
    	pub_1_->unlockAndPublish();
    }

    pub_2_->msg_.data = (long)(ret_blink);
    pub_2_->unlockAndPublish();*/
}

/****************************************************************************/

void signal_handler(int signum)
{
    switch (signum) {
        case SIGALRM:
            sig_alarms++;
            break;
    }
}

static void sighand(int sig)
{
	shut_down = 1;

	return;
}

/****************************************************************************/

int main(int argc, char **argv)
{
	struct sigaction sa;
	struct itimerval tv;

	setup_rt_requirements();

	std::string node_name_ = "ethercat_node";

	ros::init(argc, argv, node_name_);
	ros::NodeHandle n;

	signal(SIGINT, sighand);

	ros::CallbackQueue queue;
	n.setCallbackQueue(&queue);

	// Set spinner
	ros::AsyncSpinner spinner(0, &queue);
	spinner.start();

	// Initialize realtime publisher
	pub_1_ = new realtime_tools::RealtimePublisher<std_msgs::Int64>(n, "ethercat_ch_1", 4);
	pub_2_ = new realtime_tools::RealtimePublisher<std_msgs::Int64>(n, "ethercat_ch_2", 4);

    sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;

    if (sigaction(SIGALRM, &sa, 0))
    {
        fprintf(stderr, "Failed to install signal handler!\n");
        return -1;
    }

    printf("Starting timer...\n");
    tv.it_interval.tv_sec = 0;
    tv.it_interval.tv_usec = 1000000 / FREQUENCY;
    tv.it_value.tv_sec = 0;
    tv.it_value.tv_usec = 1000;

    if (setitimer(ITIMER_REAL, &tv, NULL))
    {
        fprintf(stderr, "Failed to start timer: %s\n", strerror(errno));
        return 1;
    }

    printf("Started.\n");
    while (!shut_down)
    {
        pause();

        while (sig_alarms != user_alarms)
        {
            cyclic_task();
            user_alarms++;
        }
    }

    spinner.stop();

    clean_up();

    ROS_INFO("Node shutting down...");

    ros::shutdown();

    return 0;
}

/****************************************************************************/
