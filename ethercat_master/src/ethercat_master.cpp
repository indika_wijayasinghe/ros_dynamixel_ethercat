/*
 *	Author:	Indika Wijayasinghe
 *	Email:	i.b.wijayasinghe@ieee.org
 */

#include <ethercat_master/ethercat_master.h>
#include <ethercat_master/easycat.h>
#include <stdio.h>

namespace ethercat_master
{
	EtherCATMaster::EtherCATMaster()
	{
		ec_slave_config_t *sc;

		master = ecrt_request_master(0);
		if (!master)
			printf("Requesting master failed...\n");

		domain1 = ecrt_master_create_domain(master);
		if (!domain1)
			printf("Domain creation failed...\n");

		if (!(sc = ecrt_master_slave_config(master, Pos[0], Pos[1], ID[0], ID[1])))
		{
			fprintf(stderr, "Failed to get EasyCAT slave configuration.\n");
		}

		if (ecrt_domain_reg_pdo_entry_list(domain1, domain1_regs))
		{
			fprintf(stderr, "PDO entry registration failed!\n");
		}

		printf("EasyCAT PDO registered\n");

		printf("Activating master...\n");
		if (ecrt_master_activate(master))
			printf("Failed to activate master...\n");

		printf("Activation successful...\n");

		if (!(domain1_pd = ecrt_domain_data(domain1)))
			printf("Domain data error...\n");

		off_in = off_easycat_in;
		off_out = off_easycat_out;

		printf("Offsets (i,o): %d %d\n", off_in, off_out);
	}

	EtherCATMaster::~EtherCATMaster() {	}


	void EtherCATMaster::sendDatagram()
	{
		for(int i = 0; i < 32; i++)
		{
			EC_WRITE_U8(domain1_pd + off_easycat_out + i, domain_pd[off_easycat_out + i]);
		}

		ecrt_domain_queue(domain1);
		ecrt_master_send(master);
	}

	void EtherCATMaster::receiveDatagram()
	{
		ecrt_master_receive(master);
		ecrt_domain_process(domain1);

		for(int i = 0; i < 32; i++)
		{
			domain_pd[off_easycat_in + i] = (unsigned int)EC_READ_U8(domain1_pd + off_easycat_in + i);
		}
	}

	void EtherCATMaster::check_domain1_state(void)
	{
	    ec_domain_state_t ds;

	    ecrt_domain_state(domain1, &ds);

	    if (ds.working_counter != domain1_state.working_counter)
	        printf("Domain1: WC %u.\n", ds.working_counter);
	    if (ds.wc_state != domain1_state.wc_state)
	        printf("Domain1: State %u.\n", ds.wc_state);

	    domain1_state = ds;
	}

	void EtherCATMaster::check_master_state(void)
	{
	    ec_master_state_t ms;

	    ecrt_master_state(master, &ms);

	    if (ms.slaves_responding != master_state.slaves_responding)
	        printf("%u slave(s).\n", ms.slaves_responding);
	    if (ms.al_states != master_state.al_states)
	        printf("AL states: 0x%02X.\n", ms.al_states);
	    if (ms.link_up != master_state.link_up)
	        printf("Link is %s.\n", ms.link_up ? "up" : "down");

	    master_state = ms;
	}

	void EtherCATMaster::check_slave_config_states(void)
	{
	    ec_slave_config_state_t s;

	    ecrt_slave_config_state(sc_easycat_in, &s);

	    if (s.al_state != sc_easycat_in_state.al_state)
	        printf("EasyCAT_In: State 0x%02X.\n", s.al_state);
	    if (s.online != sc_easycat_in_state.online)
	        printf("EasyCAT_In: %s.\n", s.online ? "online" : "offline");
	    if (s.operational != sc_easycat_in_state.operational)
	        printf("EasyCAT_In: %soperational.\n",
	                s.operational ? "" : "Not ");

	    sc_easycat_in_state = s;
	}
}
