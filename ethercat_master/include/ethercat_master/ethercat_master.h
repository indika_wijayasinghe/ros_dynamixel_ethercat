/*
 *	Author:	Indika Wijayasinghe
 *	Email:	i.b.wijayasinghe@ieee.org
 */

#include <ethercat_master/ecrt.h>
#include <errno.h>

namespace ethercat_master
{

class EtherCATMaster
{
public:
	unsigned int domain_pd[64];
	unsigned int off_in;
	unsigned int off_out;
	ec_master_state_t master_state = {};
	ec_domain_state_t domain1_state = {};
	ec_slave_config_state_t sc_easycat_in_state = {};

	EtherCATMaster();
	~EtherCATMaster();

	// Send data
	void sendDatagram();

	// Receive data
	void receiveDatagram();

	void check_domain1_state(void);
	void check_master_state(void);
	void check_slave_config_states(void);

private:
	ec_master_t *master = NULL;
	ec_domain_t *domain1 = NULL;
	ec_slave_config_t *sc_easycat_in = NULL;

	// Process data
	uint8_t *domain1_pd = NULL;
};

}
