cmake_minimum_required(VERSION 2.8.3)
project(ethercat_master)

add_definitions(-std=c++11)

find_package(catkin REQUIRED COMPONENTS
  roscpp
  rospy
  std_msgs
  realtime_tools
)

catkin_package(
  INCLUDE_DIRS include
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

SET(GCC_COVERAGE_COMPILE_FLAGS "-I/opt/etherlab/include")
SET(GCC_COVERAGE_LINK_FLAGS    "-L/opt/etherlab/lib/ -lethercat")
SET(CMAKE_CXX_FLAGS  "${CMAKE_CXX_FLAGS} ${GCC_COVERAGE_COMPILE_FLAGS}")
SET(CMAKE_EXE_LINKER_FLAGS  "${CMAKE_EXE_LINKER_FLAGS} ${GCC_COVERAGE_LINK_FLAGS}")
SET(ETHERCAT_LIB "/opt/etherlab/lib")

add_library(ethercat_master_lib src/ethercat_master.cpp)
add_executable(ethercat_master_test src/ethercat_master_test.cpp)
target_link_libraries(ethercat_master_test ${catkin_LIBRARIES} ethercat_master_lib ethercat)