# README #

## What is this repository for? ##

* This package provides controller and hardware interfaces for latency analysis of a simple realtime controller for Dyanmixel motors. 
* v0.3
* Currently contains a simple PID controller, and almost done hardware interface for Dynamixel
* Two examples are included; one for testing with RRBOT demo in Gazebo, and the other for testing with real Dynamixel motors.

## How do I get set up? ##

### Simulation with Gazebo and RRBOT ###

* Follow [Tutorial: ROS Control](http://gazebosim.org/tutorials/?tut=ros_control) to setup RRBOT in Gazebo.
* Use the following commands to launch RRBOT and PID controller
```
roslaunch rrbot_gazebo rrbot_world.launch
roslaunch arna_controllers_pkg_demo arna_controller.launch
```
* Use the following commands format to issue position commands to the two joints of RRBOT
```
rostopic pub -1 /rrbot/joint1_position_controller/command std_msgs/Float64 "data: 1.5"
rostopic pub -1 /rrbot/joint2_position_controller/command std_msgs/Float64 "data: 1.5"
```
* PID gains for the controller can be changed in "arna_controllers_pkg_demo/config/rrbot_arna_controller_demo.yaml".

### With real hardware interface to Dynamixel ###

* Use the following commands to launch Dynamixel interface with PID controller
```
rosrun arna_hardware_control_demo two_link_robot
roslaunch arna_hardware_control_demo arna_controller.launch
```
* Use the following commands format to issue position commands to the two joints of RRBOT
```
rostopic pub -1 /two_link_robot/joint1_position_controller/command std_msgs/Float64 "data: 1.5"
rostopic pub -1 /two_link_robot/joint2_position_controller/command std_msgs/Float64 "data: 1.5"
```
* PID gains for the controller can be changed in "arna_hardware_control_demo/config/two_link_robot_control.yaml".

## Who do I talk to? ##

* Indika Wijayasinghe: <i.b.wijayasinghe@ieee.org>
