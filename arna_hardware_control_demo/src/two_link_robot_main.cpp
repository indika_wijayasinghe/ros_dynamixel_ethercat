/*
 *	Author:	Indika Wijayasinghe
 *	Email:	i.b.wijayasinghe@ieee.org
 */
 
#include <arna_controllers/position_controller.h>
#include <arna_hardware_interfaces/arm_hardware.h>
#include <controller_manager/controller_manager.h>
#include <ros/ros.h>
#include <ros/time.h>
#include <ros/callback_queue.h>
#include <realtime_tools/realtime_publisher.h>

#include <sstream>
#include <cmath>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sched.h>
#include <string.h>
#include <pthread.h>
#include <limits.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/mman.h>

#define MY_PRIORITY (49) // We use 49 as the PRREMPT_RT use 50 as the priority of kernel tasklets and interrupt handler by default
#define MAX_SAFE_STACK (8*1024) // The maximum stack size which is guaranteed safe to access without faulting
#define NSEC_PER_SEC (1000000000)
#define SPEID (4)	// CPU ID to pin the thread
#define	CONTROLLER_FREQ (500) // Update rate of the controller in Hz

static int shut_down = 0;
static int latency_target_fd;
static long max_latency = 0;

/*
 * Latency trick (Copied from cyclictest source code) if the file /dev/cpu_dma_latency exists, open it and write a zero into it. This will tell
 * the power management system not to transition to a high cstate (in fact, the system acts like idle=poll). When the fd to /dev/cpu_dma_latency
 * is closed, the behavior goes back to the system default. Documentation/power/pm_qos_interface.txt
 */
static void set_latency_target(void)
{
	struct stat s;
	int err;
	latency_target_fd = -1;
	int32_t latency_target_value = 0;

	errno = 0;
	err = stat("/dev/cpu_dma_latency", &s);

	if (err == -1)
	{
		perror("Could not stat /dev/cpu_dma_latency failed");
		return;
	}

	errno = 0;
	latency_target_fd = open("/dev/cpu_dma_latency", O_RDWR);

	if (latency_target_fd == -1)
	{
		perror("Could not open /dev/cpu_dma_latency");
		return;
	}

	errno = 0;
	err = write(latency_target_fd, &latency_target_value, 4);

	if (err < 1)
	{
		perror("error setting cpu_dma_latency to %d!");
		close(latency_target_fd);
		return;
	}

	printf("/dev/cpu_dma_latency set to %dus\n", latency_target_value);
}

static void sighand(int sig)
{
	shut_down = 1;

	return;
}

static void setup_rt_requirements(void)
{
	struct sched_param param;
	unsigned char dummy[MAX_SAFE_STACK];

	cpu_set_t cpuset;
	pthread_t thread;

	thread = pthread_self();

	// Set affinity mask to include CPUs 0 to 7
	CPU_ZERO(&cpuset);
	CPU_SET(SPEID, &cpuset);

	if (pthread_setaffinity_np(thread, sizeof(cpu_set_t), &cpuset) == -1) 
	{
		perror("pthread_setaffinity failed");
	}

	if (pthread_getaffinity_np(thread, sizeof(cpu_set_t), &cpuset) == -1) 
	{
		perror("pthread_getaffinity_np");
	}

	for (int j = 0; j < CPU_SETSIZE; j++) 
	{
		if (CPU_ISSET(j, &cpuset))
		{
	   		fprintf(stderr,"%d CPU %d\n", SPEID, j);
		}
	}

	// Declare ourself as a real time task
	param.sched_priority = MY_PRIORITY;

	if (sched_setscheduler(0, SCHED_FIFO, &param) == -1)
	{
		perror("sched_setscheduler failed");
		exit(-1);
	}

	// Lock memory
	if (mlockall(MCL_CURRENT|MCL_FUTURE) == -1)
	{
		perror("mlockall failed");
	    exit(-2);
	}

	// Use the /dev/cpu_dma_latency trick available
	set_latency_target();

	// Pre-fault our stack
	memset(dummy, 0, MAX_SAFE_STACK);

	return;
}

static void clean_up()
{
	munlockall();

	if (latency_target_fd >= 0)
		close(latency_target_fd);

	return;
}

static bool stop_controllers(controller_interface::ControllerBase* cb[], ros::Time time_)
{
	bool running = true;

	running &= cb[0]->stopRequest(time_);
	running &= cb[1]->stopRequest(time_);
	running &= cb[2]->stopRequest(time_);

	return running;
}

static bool start_controllers(controller_interface::ControllerBase* cb[], ros::Time time_)
{
	bool running = true;

	running &= cb[0]->startRequest(time_);
	running &= cb[1]->startRequest(time_);
	running &= cb[2]->startRequest(time_);

	return running;
}

static struct timespec calculate_nextshot(struct timespec t, int nano_step)
{
	struct timespec t_n = t;

	t_n.tv_nsec += nano_step;

	while (t_n.tv_nsec >= NSEC_PER_SEC)
	{
		t_n.tv_nsec -= NSEC_PER_SEC;
		t_n.tv_sec++;
	}

	return t_n;
}

static void handle_op_status(int event, controller_interface::ControllerBase* cb[], ros::Time time_)
{
	bool ret;

	switch (event)
	{
	case 1:
		ret = start_controllers(cb, time_);
		max_latency = 0;
		break;
	case -1:
		ret = stop_controllers(cb, time_);
		break;
	default:
		break;
	}
}

int main(int argc, char **argv)
{
	setup_rt_requirements();

	struct timespec t, t_o, t_m;
	long prev, curr;

	std::string node_name_ = "two_link_arm_node";

	// Controller frequency and time step
	int interval = 1/((double)CONTROLLER_FREQ)*NSEC_PER_SEC;
	int interval_offset = (int)((double)(interval)*5/10);
	
	ros::init(argc, argv, node_name_);
	ros::NodeHandle n;

	signal(SIGINT, sighand);
	
	ros::CallbackQueue queue;
  	n.setCallbackQueue(&queue);
	
	arna_hardware_interfaces::TwoLinkArm robotArm;
	controller_manager::ControllerManager cm(&robotArm, n);

	ros::Time time_(0.0);
	ros::Duration duration_(((double)interval/(double)NSEC_PER_SEC));
	
	// Set spinner
	ros::AsyncSpinner spinner(0, &queue);
  	spinner.start();

  	// Initialize realtime publisher
  	realtime_tools::RealtimePublisher<std_msgs::Int64>* pub_1 = new realtime_tools::RealtimePublisher<std_msgs::Int64>(n, "channel_1", 4);
  	realtime_tools::RealtimePublisher<std_msgs::Int64>* pub_2 = new realtime_tools::RealtimePublisher<std_msgs::Int64>(n, "channel_2", 4);
  	realtime_tools::RealtimePublisher<std_msgs::Int64>* pub_3 = new realtime_tools::RealtimePublisher<std_msgs::Int64>(n, "channel_3", 4);
  	realtime_tools::RealtimePublisher<std_msgs::Int64>* pub_4 = new realtime_tools::RealtimePublisher<std_msgs::Int64>(n, "channel_4", 4);
  	realtime_tools::RealtimePublisher<std_msgs::Int64>* pub_5 = new realtime_tools::RealtimePublisher<std_msgs::Int64>(n, "latency", 4);
  	realtime_tools::RealtimePublisher<std_msgs::Int64>* pub_6 = new realtime_tools::RealtimePublisher<std_msgs::Int64>(n, "controller_status", 4);

  	// Load controllers
  	cm.loadController("/two_link_robot/joint_state_controller");
  	cm.loadController("/two_link_robot/joint1_position_controller");
  	cm.loadController("/two_link_robot/joint2_position_controller");

  	// Controller bases
  	controller_interface::ControllerBase* cb[3];
  	cb[0] = cm.getControllerByName("/two_link_robot/joint_state_controller");
  	cb[1] = cm.getControllerByName("/two_link_robot/joint1_position_controller");
  	cb[2] = cm.getControllerByName("/two_link_robot/joint2_position_controller");

  	// Controller status
  	bool running = false;

	clock_gettime(CLOCK_MONOTONIC, &t);
	prev = 1000000000*t.tv_sec + t.tv_nsec;

    while(!shut_down)
    {
    	// Wait until next shot
    	t = calculate_nextshot(t, interval);
    	clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t, NULL);

        // -- Start: RT stuff --
    	clock_gettime(CLOCK_MONOTONIC, &t_m);
    	curr = 1000000000*t_m.tv_sec + t_m.tv_nsec;

    	robotArm.read();
    	cm.update(time_, duration_);

    	// Check for EtherCAT interface status change
    	handle_op_status(robotArm.op_event, cb, time_);

    	// Are the controllers running?
    	running = ((cb[0]->isRunning()) & (cb[1]->isRunning()) & (cb[2]->isRunning()));

    	// Wait until write shot
    	t_o = calculate_nextshot(t, interval_offset);
    	clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &t_o, NULL);

    	if (running)
    		robotArm.write();

    	if (max_latency < abs(curr-prev-interval))
    		max_latency = abs(curr-prev-interval);

    	pub_1->msg_.data = abs((int)(robotArm.pos[0]*57.30));
    	pub_2->msg_.data = abs((int)(robotArm.pos[1]*57.30));
    	pub_3->msg_.data = abs((int)(robotArm.cmd[0]));
    	pub_4->msg_.data = abs((int)(robotArm.cmd[1]));
    	pub_5->msg_.data = max_latency;
    	pub_6->msg_.data = (int)robotArm.op_status;
    	pub_1->unlockAndPublish();
    	pub_2->unlockAndPublish();
    	pub_3->unlockAndPublish();
    	pub_4->unlockAndPublish();
    	pub_5->unlockAndPublish();
    	pub_6->unlockAndPublish();
    	prev = curr;
    	// -- End: RT stuff --
    }

    spinner.stop();

    clean_up();

    ROS_INFO("Node shutting down...");

    ros::shutdown();

    return 0;
}
