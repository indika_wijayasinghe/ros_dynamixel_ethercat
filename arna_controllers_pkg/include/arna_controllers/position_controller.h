/*
 *	Author: Indika Wijayasinghe
 *	Email:	i.b.wijayasinghe@ieee.org
 */

#include <ros/node_handle.h>
#include <controller_interface/controller.h>
#include <hardware_interface/joint_command_interface.h>
//#include <realtime_tools/realtime_publisher.h>
#include <realtime_tools/realtime_buffer.h>
#include <std_msgs/Int64.h>
#include <std_msgs/Float64.h>

namespace arna_controllers
{

class PositionController : public controller_interface::Controller<hardware_interface::EffortJointInterface>
{
public:
	// Structure for command signal
	struct Commands			
	{
		double position_;	// Last commanded position
	};
	
	// Structure for PID gains
	struct Gains
	{
		double p_gain_;
		double i_gain_;
		double d_gain_;
	};
	
	PositionController();
	~PositionController();

  	bool init(hardware_interface::EffortJointInterface* hw, ros::NodeHandle &n);
  	
  	void update(const ros::Time& time, const ros::Duration& period);
  	void starting(const ros::Time& time);
  	void stopping(const ros::Time& time);
  	
  	// Set position command	
  	void setCommand(double pos_command);	
  	
  	// Set and get PID gains
  	void setGains(const double &p, const double &i, const double &d, const double &i_max, const double &i_min);
  	void getGains(double &p, double &i, double &d, double &i_max, double &i_min);
  	
private:  	
  	hardware_interface::JointHandle joint_;
  	realtime_tools::RealtimeBuffer<Commands> command_;	// Realtime buffer
  	Commands command_struct_;	// Pre-allocated memory that is re-used to set the realtime buffer
  	ros::Subscriber sub_command_;	// Subscriber for command
  	//realtime_tools::RealtimePublisher<std_msgs::Int64>* pub_;	// Realtime publisher for any publishing from update

  	Gains pid_gains_;	
  	std::string joint_name_;
  	
  	double time_step_; // Time step for updates
  	double effort[2] = {0.0, 0.0};	// Control torque with history
  	double error[3] = {0.0, 0.0, 0.0};	// Error with history
  	
  	bool sim_; // Is this a simulation?
  	
  	void setCommandCB(const std_msgs::Float64ConstPtr& msg);
};

}
