/*
 *	Author: Indika Wijayasinghe
 *	Email:	i.b.wijayasinghe@ieee.org
 */

#include <arna_controllers/position_controller.h>
#include <pluginlib/class_list_macros.h>

namespace arna_controllers
{
	PositionController::PositionController() { }
	
	PositionController::~PositionController()
	{
		sub_command_.shutdown();
	}

	// Controller initialization on load
	bool PositionController::init(hardware_interface::EffortJointInterface* hw, ros::NodeHandle &n)
  	{
		// Get joint name from the parameter server
    	if (!n.getParam("joint", joint_name_))
    	{
      		ROS_ERROR("Could not find joint name");
      		return false;
    	}

    	// Get PID gains from the parameter server
    	ros::NodeHandle nh(ros::NodeHandle(n, "pid"));
    		
    	// Optional gains (defaults to 0)
    	nh.param("p", pid_gains_.p_gain_, 0.0);
    	nh.param("i", pid_gains_.i_gain_, 0.0);
    	nh.param("d", pid_gains_.d_gain_, 0.0);
    		
    	// Start command subscriber to obtain desired torque values
    	sub_command_ = n.subscribe<std_msgs::Float64>("command", 1, &PositionController::setCommandCB, this);

    	// Get the joint object to use in the realtime loop
    	joint_ = hw->getHandle(joint_name_);  // throws on failure
    	return true;
  	}

	// Controller update function: A simple PID controller implemented
  	void PositionController::update(const ros::Time& time, const ros::Duration& period)
  	{
  		command_struct_ = *(command_.readFromRT());

  		// Values pos, vel, and eff from hardware interface appears here as joint_.getPositon(), joint_.getVelocity(), and joint_.getEffort().
  		error[0] = command_struct_.position_ - joint_.getPosition();	
  		
  		time_step_ = period.toSec();

  		// Discrete time PID controller update
  		effort[0] = effort[1] + (pid_gains_.p_gain_+pid_gains_.i_gain_*time_step_+pid_gains_.d_gain_/time_step_)*error[0]-(pid_gains_.p_gain_+2*pid_gains_.d_gain_/time_step_)*error[1]+(pid_gains_.d_gain_/time_step_)*error[2];
  		
  		// Set joint torque. This value appears in the hardware interface as cmd
  		joint_.setCommand(effort[0]);

  		// Elapse one time step for previous values
  		error[2] = error[1];
  		error[1] = error[0];
  		effort[1] = effort[0];
  	}

	// Executed at start of controller. Set desired joint position to current position.
  	void PositionController::starting(const ros::Time& time) 
  	{
  		double pos_command = joint_.getPosition();
  	
  		command_struct_.position_ = pos_command;
  		command_.initRT(command_struct_); 
  	}
  	
  	// Set desired joint position
  	void PositionController::setCommand(double pos_command)
  	{
  		command_struct_.position_ = pos_command;
  		command_.writeFromNonRT(command_struct_);
  	}
  	
  	void PositionController::stopping(const ros::Time& time) { }
  	
  	void PositionController::setCommandCB(const std_msgs::Float64ConstPtr& msg)
  	{
  		setCommand(msg->data);
  	}

PLUGINLIB_EXPORT_CLASS(arna_controllers::PositionController, controller_interface::ControllerBase);
}


